﻿using ImageMapper_Module;
using ImageMapperTester_Module.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ImageMapperTester_Module
{
    public class MainWindowViewModel : ViewModelBase
    {
        OntologyModDBConnector dbReader_ImageList;
        OntologyModDBConnector dbReader_ImageLists;
        Globals globals;
        ImageMapperEngine imageMapperEngine;

        private List<ImageItem> imageList = new List<ImageItem>();
        public List<ImageItem> ImageList
        {
            get { return imageList; }
            set
            {
                imageList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedImageItem);
                RaisePropertyChanged(NotifyChanges.MainWindow_ImageList);
            }
        }

        public ImageItem SelectedImageItem
        {
            get
            {
                if (ImageList != null && ImageList.Count > ImageIndex)
                {
                    return ImageList[ImageIndex];
                }
                else
                {
                    return null;
                }
            }
        }

        private List<clsOntologyItem> imageLists = new List<clsOntologyItem>();
        public List<clsOntologyItem> ImageLists
        {
            get { return imageLists; }
            set
            {
                imageLists = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_ImageLists);
            }
        }

        private int imageIndex = 0;
        public int ImageIndex
        {
            get { return imageIndex; }
            set
            {
                imageIndex = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedImageItem);
                RaisePropertyChanged(NotifyChanges.MainWindow_ImageIndex);
            }
        }


        public MainWindowViewModel()
        {
            Initialize();
        }

        private void ImageMapperEngine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (e.PropertyName == imageMapperEngine.NotifyChange_ListLoaded)
            {
                
                var imageSourceConverter = new ImageSourceConverter();
                   
                if (imageMapperEngine.ListLoaded)
                {
                    
                    ImageList = imageMapperEngine.ImageItemList;
                    
                }
            }
        }

        private clsOntologyItem GetDataImageLists()
        {
            var searchImageLists = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = imageMapperEngine.OItem_class_image_lists.GUID
                }
            };

            var result = dbReader_ImageLists.GetDataObjects(searchImageLists);

            return result;
        }

        public void GetImageGrid(string idImageList)
        {

            var ontologyTypeTree = dbReader_ImageList.GetOItem(idImageList, globals.Type_Object);

            imageMapperEngine.GetImagesOfImageList(ontologyTypeTree);
        }

        private void Initialize()
        {
            globals = new Globals();
            imageMapperEngine = new ImageMapperEngine(globals);
            imageMapperEngine.PropertyChanged += ImageMapperEngine_PropertyChanged;
            dbReader_ImageList = new OntologyModDBConnector(globals);
            dbReader_ImageLists = new OntologyModDBConnector(globals);

            var result = GetDataImageLists();

            if (result.GUID == globals.LState_Success.GUID)
            {
                ImageLists = dbReader_ImageLists.Objects1.OrderBy(imageItem => imageItem.Name).ToList();
            }
        }
    }
}
