﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ImageMapper_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System.IO;

namespace ImageMapperTester_Module
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

       

        public MainWindow()
        {
            InitializeComponent();
            
        }

        

        

        private void button_Click(object sender, RoutedEventArgs e)
        {

            var viewModel = (MainWindowViewModel)DataContext;

            viewModel.GetImageGrid("b06b021811fa48c5a23b38707c58327f");
            
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var viewModel = (MainWindowViewModel)DataContext;


        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.AddedItems.Cast<clsOntologyItem>().FirstOrDefault();
            var viewModel = (MainWindowViewModel)DataContext;
            viewModel.GetImageGrid(item.GUID);
        }
    }
}
