﻿using ImageMapper_Module;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageMapperTester_Module.Converter
{
    public class ImageItemToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var imageItems = (List<ImageItem>)value;

            return imageItems.Select(imageItem =>
            {
                var bitmapImage = new BitmapImage();
                var memoryStream = new MemoryStream();
                imageItem.ImageOfItem.Save(memoryStream, imageItem.ImageOfItem.RawFormat);

                memoryStream.Seek(0, SeekOrigin.Begin);

                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;

                bitmapImage.EndInit();

                var image = new Image();
                image.Uid = imageItem.IdOImage;
                image.Source = bitmapImage;
                return image;
            }).ToList();
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemList = (List<Image>)value;

            return itemList.Select(img => new ImageItem { IdOImage = img.Uid }).ToList();
        }
    }
}
