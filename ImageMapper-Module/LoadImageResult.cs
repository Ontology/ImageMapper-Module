﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageMapper_Module
{
    public class LoadImageResult
    {
        public Image ImageItem {get; set;}
        public clsOntologyItem LoadResult { get; set; }
    }

}
