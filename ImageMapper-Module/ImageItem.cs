﻿using ImageMapper_Module.BaseClasses;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageMapper_Module
{
    public enum ImageItemType
    {
        None = 0,
        KindOfOntology = 1,
        Reference = 2
    }
    public class ImageItem : NotifyPropertyChange
    {

        public bool IsRoot
        {
            get { return oARoot != null ? oARoot.Val_Bit != null ? oARoot.Val_Bit.Value : false : false; }
            
        }

        private clsObjectAtt oARoot;
        public clsObjectAtt OARoot
        {
            get { return oARoot; }
            set
            {
                oARoot = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_IsRoot);
            }
        }

        private clsOntologyItem oImage;
        public clsOntologyItem OImage
        {
            get { return oImage; }
            set
            {
                oImage = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_OImage);
                RaisePropertyChanged(NotifyChanges.ImageItem_IdOImage);
                RaisePropertyChanged(NotifyChanges.ImageItem_NameOImage);
            }
        }

        public string IdOImage
        {
            get
            {
                return OImage.GUID;
            }
            set
            {
                OImage.GUID = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_IdOImage);
            }
        }

        public string NameOImage
        {
            get
            {
                return OImage.Name;
            }
            set
            {
                OImage.Name = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_NameOImage);
            }
        }

        private clsOntologyItem kindOfOntologyItem;
        public clsOntologyItem KindOfOntologyItem
        {
            get { return kindOfOntologyItem; }
            set
            {
                kindOfOntologyItem = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_KindOfOntologyItem);
                RaisePropertyChanged(NotifyChanges.ImageItem_IdKindOfOntology);
                RaisePropertyChanged(NotifyChanges.ImageItem_NameKindOfOntology);
            }
        }

        public string IdKindOfOntology
        {
            get { return kindOfOntologyItem!= null ? kindOfOntologyItem.GUID : null; }
        }

        public string NameKindOfOntology
        {
            get { return kindOfOntologyItem != null ? kindOfOntologyItem.Name : null; }
        }

        public ImageItemType ImageItemType
        {
            get
            {
                if (kindOfOntologyItem != null) return ImageItemType.KindOfOntology;
                if (referenceItem != null) return ImageItemType.Reference;
                return ImageItemType.None;
            }
        }

        private clsOntologyItem referenceItem;
        public clsOntologyItem ReferenceItem
        {
            get { return referenceItem; }
            set
            {
                referenceItem = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_ReferenceItem);
                RaisePropertyChanged(NotifyChanges.ImageItem_IdReferenceItem);
                RaisePropertyChanged(NotifyChanges.ImageItem_NameReferenceItem);
                RaisePropertyChanged(NotifyChanges.ImageItem_IdParentReferenceItem);
                RaisePropertyChanged(NotifyChanges.ImageItem_OntologyOfReferenceItem);
            }
        }

        public string IdReferenceItem
        {
            get { return referenceItem != null ?  referenceItem.GUID : null; }
            set
            {
                if (referenceItem == null) throw new Exception("No ReferenceItem");
                referenceItem.GUID = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_IdReferenceItem);
            }
        }

        public string NameReferenceItem
        {
            get { return referenceItem != null ? referenceItem.Name : null; }
            set
            {
                if (referenceItem == null) throw new Exception("No ReferenceItem");
                referenceItem.Name = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_NameReferenceItem);
            }
        }

        public string IdParentReferenceItem
        {
            get { return referenceItem != null ? referenceItem.GUID_Parent : null; }
            set
            {
                if (referenceItem == null) throw new Exception("No ReferenceItem");
                referenceItem.GUID_Parent = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_IdParentReferenceItem);
            }
        }

        public string OntologyOfReferenceItem
        {
            get { return referenceItem != null ? referenceItem.Type : null; }
            set
            {
                if (referenceItem == null) throw new Exception("No ReferenceItem");
                referenceItem.Type = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_OntologyOfReferenceItem);
            }
        }

        private clsOntologyItem oMediaItem;
        public clsOntologyItem OMediaItem
        {
            get { return oMediaItem; }
            set
            {
                oMediaItem = value;
            }
        }

        private clsOntologyItem oFileOfMediaItem;
        public clsOntologyItem OFileOfMediaItem
        {
            get { return oFileOfMediaItem; }
            set
            {
                oFileOfMediaItem = value;
            }
        }

        private LoadImageResult loadImageResult;
        public LoadImageResult LoadImageResult
        {
            get { return loadImageResult; }
            set
            {
                loadImageResult = value;
                if (loadImageResult != null && loadImageResult.ImageItem != null)
                {
                    ThumbNail_16x16 = GetThumbNail(16, 16);
                    ThumbNail_32x32 = GetThumbNail(32, 32);
                }
                else
                {
                    ThumbNail_16x16 = null;
                    ThumbNail_32x32 = null;
                }
                RaisePropertyChanged(NotifyChanges.ImageItem_ImageOfItem);
            }
        }

        private clsOntologyItem imageState;
        public clsOntologyItem ImageState
        {
            get { return imageState; }
            set
            {
                imageState = value;
                RaisePropertyChanged(NotifyChanges.ImageItem_ImageState);
            }
        }

        public Image ImageOfItem
        {
            get { return LoadImageResult != null ? LoadImageResult.ImageItem : null; }
        }

        private Image thumbNail_16x16;
        public Image ThumbNail_16x16
        {
            get { return thumbNail_16x16; }
            set
            {
                thumbNail_16x16 = value;
            }
        }

        private Image thumbNail_32x32;
        public Image ThumbNail_32x32
        {
            get { return thumbNail_32x32; }
            set
            {
                thumbNail_32x32 = value;
            }
        }

        public Image GetThumbNail(int width, int height)
        {
            var dummyDeleg = new Image.GetThumbnailImageAbort(thumbDeleg);
            return ImageOfItem != null ? ImageOfItem.GetThumbnailImage(width, height, dummyDeleg, IntPtr.Zero) : null;
        }

        private bool thumbDeleg()
        {
            return false;
        }
    }
}
