﻿using ImageMapper_Module.BaseClasses;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ImageMapper_Module
{
    public class ImageMapperEngine : NotifyPropertyChange
    {
        clsLocalConfig localConfig;

        private DataCollector_ImageMapper dataCollectorImageMapper;
        private FileWorkManager fileWorkManager;
        private MediaStoreConnector mediaStoreConnector;

        public string NotifyChange_ListLoaded
        {
            get { return NotifyChanges.ImageMapperEngine_ListLoaded; }
        }

        public string NotifyChange_ImageItemList
        {
            get { return NotifyChanges.ImageMapperEngine_ImageItemList; }
        }

        private bool listLoaded;
        public bool ListLoaded
        {
            get { return listLoaded; }
            set
            {
                listLoaded = value;
                RaisePropertyChanged(NotifyChanges.ImageMapperEngine_ListLoaded);
            }
        }

        public clsOntologyItem OItem_attributetype_is_root
        {
            get { return localConfig.OItem_attributetype_is_root; }
        }

        public clsOntologyItem OItem_class_image_lists
        {
            get { return localConfig.OItem_class_image_lists; }
        }

        public clsOntologyItem OItem_class_images__image_lists_
        {
            get { return localConfig.OItem_class_images__image_lists_; }
        }

        public clsOntologyItem OItem_class_kindofontology
        {
            get { return localConfig.OItem_class_kindofontology; }
        }

        public clsOntologyItem OItem_KindOfOntology_Attribute
        {
            get { return localConfig.OItem_object_attribute; }
        }

        public clsOntologyItem OItem_KindOfOntology_Class
        {
            get { return localConfig.OItem_object_class; }
        }

        public clsOntologyItem OItem_KindOfOntology_Object
        {
            get { return localConfig.OItem_object_object; }
        }

        public clsOntologyItem OItem_KindOfOntology_RelationType
        {
            get { return localConfig.OItem_object_relationtype; }
        }

        public clsOntologyItem OItem_relationtype_belonging_resource
        {
            get { return localConfig.OItem_relationtype_belonging_resource; }
        }

        public clsOntologyItem OItem_relationtype_contains
        {
            get { return localConfig.OItem_relationtype_contains; }
        }

        public clsOntologyItem OItem_relationtype_reference_to
        {
            get { return localConfig.OItem_relationtype_reference_to; }
        }

        public clsOntologyItem OItem_ImageState_Opened
        {
            get { return localConfig.OItem_object_opened; }
        }

        public clsOntologyItem OItem_ImageState_Closed
        {
            get { return localConfig.OItem_object_closed; }
        }

        private List<ImageItem> imageItemList;
        public List<ImageItem> ImageItemList
        {
            get { return imageItemList; }
            set
            {
                imageItemList = value;
                RaisePropertyChanged(NotifyChanges.ImageMapperEngine_ImageItemList);
            }
        }

        public clsOntologyItem GetImagesOfImageList(clsOntologyItem imageList)
        {

            var result = dataCollectorImageMapper.GetData_ImagesOfImageList(imageList);

            if (result.GUID == localConfig.Globals.LState_Success.GUID || result.GUID == localConfig.Globals.LState_Nothing.GUID)
            {
                result = localConfig.Globals.LState_Success.Clone();
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }
            return result;
        }

        public ImageMapperEngine(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            dataCollectorImageMapper = new DataCollector_ImageMapper(localConfig);
            dataCollectorImageMapper.PropertyChanged += DataCollectorImageMapper_PropertyChanged;
            mediaStoreConnector = new MediaStoreConnector(localConfig.Globals);
            fileWorkManager = new FileWorkManager(localConfig.Globals);
        }

        private LoadImageResult LoadImage(clsOntologyItem fileItem)
        {
            string path;
            var result = localConfig.Globals.LState_Success.Clone();
            Image imageItem = null;
            if (fileWorkManager.IsBlobFile(fileItem))
            {
                path = @"%TEMP%\" + Guid.NewGuid().ToString();
                path = Environment.ExpandEnvironmentVariables(path);
                result = mediaStoreConnector.SaveManagedMediaToFile(fileItem, path, true, true);
            }
            else
            {
                path = fileWorkManager.GetPathFileSystemObject(fileItem);
                if (!File.Exists(path))
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                try
                {
                    imageItem = Image.FromFile(path);
                }
                catch(Exception ex)
                {
                    result = localConfig.Globals.LState_Error.Clone();
                }
                
            }

            return new LoadImageResult { ImageItem = imageItem, LoadResult = result };
        }

        private void DataCollectorImageMapper_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.ImageMapperEngine_Result_ImagesOfImageList)
            {
                
                if (dataCollectorImageMapper.Result_ImagesOfImageList.GUID == localConfig.Globals.LState_Nothing.GUID)
                {
                    ListLoaded = false;
                }
                else if (dataCollectorImageMapper.Result_ImagesOfImageList.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    
                    ImageItemList = new List<ImageItem>();
                    ListLoaded = false;
                }
                else if (dataCollectorImageMapper.Result_ImagesOfImageList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    ImageItemList = (from imageList in dataCollectorImageMapper.ImageLists
                                     join imageItem in dataCollectorImageMapper.ImagesOfImageList on imageList.GUID equals imageItem.ID_Object
                                     join imageState in dataCollectorImageMapper.StatesOfImages on imageItem.ID_Other equals imageState.ID_Object into imageStates
                                     from imageState in imageStates.DefaultIfEmpty()
                                     join isRoot in dataCollectorImageMapper.ImagesIsRoot on imageItem.ID_Other equals isRoot.ID_Object into isRoots
                                     from isRoot in isRoots.DefaultIfEmpty()
                                     join kindOfOntology in dataCollectorImageMapper.ImagesKindOfOntology on imageItem.ID_Other equals kindOfOntology.ID_Object into kindOfOntologies
                                     from kindOfOntology in kindOfOntologies.DefaultIfEmpty()
                                     join resourceItem in dataCollectorImageMapper.ImagesResources on imageItem.ID_Other equals resourceItem.ID_Object into resourceItems
                                     from resourceItem in resourceItems.DefaultIfEmpty()
                                     join mediaFile in (from mediaItem in dataCollectorImageMapper.MediaItemsOfImages
                                                        join fileOfMediaItem in dataCollectorImageMapper.FilesOfMediaItems on mediaItem.ID_Object equals fileOfMediaItem.ID_Object
                                                        select new { mediaItem, fileOfMediaItem }) on imageItem.ID_Other equals mediaFile.mediaItem.ID_Other into mediaFiles
                                     from mediaFile in mediaFiles.DefaultIfEmpty()
                                     select new ImageItem
                                     {
                                         OImage = imageItem != null ? new clsOntologyItem { GUID = imageItem.ID_Other, Name = imageItem.Name_Other, GUID_Parent = imageItem.ID_Parent_Other, Type = localConfig.Globals.Type_Object } : null,
                                         OARoot = isRoot,
                                         KindOfOntologyItem = kindOfOntology != null ? new clsOntologyItem { GUID = kindOfOntology.ID_Other, Name = kindOfOntology.Name_Other, GUID_Parent = kindOfOntology.ID_Parent_Other, Type = localConfig.Globals.Type_Object } : null,
                                         ReferenceItem = resourceItem != null ? new clsOntologyItem { GUID = resourceItem.ID_Other, Name = resourceItem.Name_Other, GUID_Parent = resourceItem.ID_Parent_Other, Type = resourceItem.Ontology } : null,
                                         LoadImageResult = mediaFile != null && mediaFile.fileOfMediaItem != null ? LoadImage(new clsOntologyItem { GUID = mediaFile.fileOfMediaItem.ID_Other, Name = mediaFile.fileOfMediaItem.Name_Other, GUID_Parent = mediaFile.fileOfMediaItem.ID_Parent_Other, Type = localConfig.Globals.Type_Object }) : null,
                                         ImageState = imageState != null ? new clsOntologyItem { GUID = imageState.ID_Other, Name = imageState.Name_Other, GUID_Parent = imageState.ID_Parent_Other, Type = imageState.Ontology} : null
                                     }).ToList();
                    ListLoaded = true;
                }
            }
            
        }
    }
}
