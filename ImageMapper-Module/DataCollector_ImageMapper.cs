﻿using ImageMapper_Module.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageMapper_Module
{
    public class DataCollector_ImageMapper : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        OntologyModDBConnector oReader_ImageList;
        OntologyModDBConnector oReader_ImagesOfList;
        OntologyModDBConnector oReader_ImageIsRoot;
        OntologyModDBConnector oReader_ImageKindOfOntology;
        OntologyModDBConnector oReader_ImageResources;
        OntologyModDBConnector oReader_ImageStates;
        OntologyModDBConnector oReader_MediaItemsOfImage;
        OntologyModDBConnector oReader_FilesOfMediaItem;


        public List<clsOntologyItem> ImageLists
        {
            get { return oReader_ImageList.Objects1; }
        }

        public List<clsObjectRel> ImagesOfImageList
        {
            get { return oReader_ImagesOfList.ObjectRels; }
        }

        public List<clsObjectAtt> ImagesIsRoot
        {
            get { return oReader_ImageIsRoot.ObjAtts; }
        }

        public List<clsObjectRel> ImagesKindOfOntology
        {
            get { return oReader_ImageKindOfOntology.ObjectRels; }
        }

        public List<clsObjectRel> ImagesResources
        {
            get { return oReader_ImageResources.ObjectRels; }
        }

        public List<clsObjectRel> MediaItemsOfImages
        {
            get { return oReader_MediaItemsOfImage.ObjectRels; }
        }

        public List<clsObjectRel> FilesOfMediaItems
        {
            get { return oReader_FilesOfMediaItem.ObjectRels; }
        }

        public List<clsObjectRel> StatesOfImages
        {
            get { return oReader_ImageStates.ObjectRels; }
        }

        private clsOntologyItem result_ImagesOfImageList;
        public clsOntologyItem Result_ImagesOfImageList
        {
            get { return result_ImagesOfImageList; }
            set
            {
                result_ImagesOfImageList = value;
                RaisePropertyChanged(NotifyChanges.ImageMapperEngine_Result_ImagesOfImageList);
            }
        }

        public bool IsActive_ImagesOfImageList
        {
            get
            {
                if (result_ImagesOfImageList == null) return false;

                if (result_ImagesOfImageList.GUID == localConfig.Globals.LState_Nothing.GUID) return true;

                return false;
            }
        }

        private System.Threading.Thread threadForImagesOfImageList;

        public clsOntologyItem GetData_ImagesOfImageList(clsOntologyItem imageList)
        {

            try
            {
                if (threadForImagesOfImageList != null)
                {
                    threadForImagesOfImageList.Abort();
                }
            }
            catch (Exception ex) { };

            Result_ImagesOfImageList = localConfig.Globals.LState_Nothing.Clone();
            Result_ImagesOfImageList.OList_Rel = new List<clsOntologyItem>();
            Result_ImagesOfImageList.OList_Rel.Add(imageList);

            threadForImagesOfImageList = new System.Threading.Thread(ThreadWorker_ImagesOfImageList);
            threadForImagesOfImageList.Start();

            return Result_ImagesOfImageList;
        }

        public void ThreadWorker_ImagesOfImageList()
        {
            var result = GetSubData_ImageList();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_ImagesOfImageList();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_ImageIsRoot();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_KindOfOntology();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_Resources();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_MediaItemOfImage();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_ImageStates();
            }

            Result_ImagesOfImageList = result.Clone();
        }

        public clsOntologyItem GetSubData_ImageList()
        {
            var imageListItem = Result_ImagesOfImageList.OList_Rel != null ? Result_ImagesOfImageList.OList_Rel.FirstOrDefault() : null;

            var searchImageList = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = imageListItem != null ? imageListItem.GUID : null,
                    GUID_Parent = localConfig.OItem_class_image_lists.GUID
                }
            };

            var result = oReader_ImageList.GetDataObjects(searchImageList);

            return result;
        }

        private clsOntologyItem GetSubData_ImagesOfImageList()
        {
            var searchImages = oReader_ImageList.Objects1.Select(imageList => new clsObjectRel
            {
                ID_Object = imageList.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_class_images__image_lists_.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImages.Any())
            {
                result = oReader_ImagesOfList.GetDataObjectRel(searchImages);
            }
            else
            {
                oReader_ImagesOfList.Objects1.Clear();
            }

            return result;
        }

        private clsOntologyItem GetSubData_ImageIsRoot()
        {
            var searchImagesIsRoot = oReader_ImagesOfList.ObjectRels.Select(imageItem => new clsObjectAtt
            {
                ID_Object = imageItem.ID_Other,
                ID_AttributeType = localConfig.OItem_attributetype_is_root.GUID
            }).ToList();

                
            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImagesIsRoot.Any())
            {
                result = oReader_ImageIsRoot.GetDataObjectAtt(searchImagesIsRoot);
            }
            else
            {
                oReader_ImageIsRoot.ObjAtts.Clear();
            }

            return result;
        }

        private clsOntologyItem GetSubData_KindOfOntology()
        {
            var searchImagesKindOfOntology = oReader_ImagesOfList.ObjectRels.Select(imageItem => new clsObjectRel
            {
                ID_Object = imageItem.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_reference_to.GUID,
                ID_Parent_Other = localConfig.OItem_class_kindofontology.GUID
            }).ToList();

                

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImagesKindOfOntology.Any())
            {
                result = oReader_ImageKindOfOntology.GetDataObjectRel(searchImagesKindOfOntology);
            }
            else
            {
                oReader_ImageKindOfOntology.ObjectRels.Clear();
            }

            return result;
        }

        private clsOntologyItem GetSubData_Resources()
        {
            var searchImagesResources = oReader_ImagesOfList.ObjectRels.Select(imageItem => new clsObjectRel
            {
                ID_Object = imageItem.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImagesResources.Any())
            {
                result = oReader_ImageResources.GetDataObjectRel(searchImagesResources);
            }
            else
            {
                oReader_ImageResources.ObjectRels.Clear();
            }

            return result;
        }

        private clsOntologyItem GetSubData_ImageStates()
        {
            var searchImageStates = oReader_ImagesOfList.ObjectRels.Select(imageItem => new clsObjectRel
            {
                ID_Object = imageItem.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is_in_state.GUID,
                ID_Parent_Other = localConfig.OItem_class_image_state.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImageStates.Any())
            {
                result = oReader_ImageStates.GetDataObjectRel(searchImageStates);
            }
            else
            {
                oReader_ImageStates.ObjectRels.Clear();
            }

            return result;
        }

        private clsOntologyItem GetSubData_MediaItemOfImage()
        {
            var searchImageMediaItem = oReader_ImagesOfList.ObjectRels.Select(imageItem => new clsObjectRel
            {
                ID_Other = imageItem.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Object = localConfig.OItem_class_images__graphic_.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchImageMediaItem.Any())
            {
                result = oReader_MediaItemsOfImage.GetDataObjectRel(searchImageMediaItem);
            }
            else
            {
                oReader_MediaItemsOfImage.ObjectRels.Clear();
                oReader_FilesOfMediaItem.ObjectRels.Clear();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchFilesOfMediaItem = oReader_MediaItemsOfImage.ObjectRels.Select(mediaItem => new clsObjectRel
                {
                    ID_Object = mediaItem.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = localConfig.OItem_class_file.GUID
                }).ToList();

                if (searchFilesOfMediaItem.Any())
                {
                    result = oReader_FilesOfMediaItem.GetDataObjectRel(searchFilesOfMediaItem);
                }
                
            }

            return result;
        }

        public DataCollector_ImageMapper(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            oReader_ImageList = new OntologyModDBConnector(localConfig.Globals);
            oReader_ImagesOfList = new OntologyModDBConnector(localConfig.Globals);
            oReader_ImageIsRoot = new OntologyModDBConnector(localConfig.Globals);
            oReader_ImageKindOfOntology = new OntologyModDBConnector(localConfig.Globals);
            oReader_ImageResources = new OntologyModDBConnector(localConfig.Globals);
            oReader_MediaItemsOfImage = new OntologyModDBConnector(localConfig.Globals);
            oReader_FilesOfMediaItem = new OntologyModDBConnector(localConfig.Globals);
            oReader_ImageStates = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
